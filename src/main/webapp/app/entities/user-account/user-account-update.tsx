import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IImage } from 'app/shared/model/image.model';
import { getEntities as getImages } from 'app/entities/image/image.reducer';
import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { IShout } from 'app/shared/model/shout.model';
import { getEntities as getShouts } from 'app/entities/shout/shout.reducer';
import { ITag } from 'app/shared/model/tag.model';
import { getEntities as getTags } from 'app/entities/tag/tag.reducer';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-account.reducer';
import { IUserAccount } from 'app/shared/model/user-account.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IUserAccountUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IUserAccountUpdateState {
  isNew: boolean;
  idslikedShout: any[];
  idstag: any[];
  idslistener: any[];
  imageId: string;
  userId: string;
  myShoutId: string;
  listensToId: string;
}

export class UserAccountUpdate extends React.Component<IUserAccountUpdateProps, IUserAccountUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      idslikedShout: [],
      idstag: [],
      idslistener: [],
      imageId: '0',
      userId: '0',
      myShoutId: '0',
      listensToId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getImages();
    this.props.getUsers();
    this.props.getShouts();
    this.props.getTags();
    this.props.getUserAccounts();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { userAccountEntity } = this.props;
      const entity = {
        ...userAccountEntity,
        ...values,
        likedShouts: mapIdList(values.likedShouts),
        tags: mapIdList(values.tags),
        listeners: mapIdList(values.listeners)
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/user-account');
  };

  render() {
    const { userAccountEntity, images, users, shouts, tags, userAccounts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="shoutApp.userAccount.home.createOrEditLabel">Create or edit a UserAccount</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : userAccountEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="user-account-id">ID</Label>
                    <AvInput id="user-account-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="usernameLabel" for="user-account-username">
                    Username
                  </Label>
                  <AvField id="user-account-username" type="text" name="username" />
                </AvGroup>
                <AvGroup>
                  <Label id="firstNameLabel" for="user-account-firstName">
                    First Name
                  </Label>
                  <AvField id="user-account-firstName" type="text" name="firstName" />
                </AvGroup>
                <AvGroup>
                  <Label id="lastNameLabel" for="user-account-lastName">
                    Last Name
                  </Label>
                  <AvField id="user-account-lastName" type="text" name="lastName" />
                </AvGroup>
                <AvGroup>
                  <Label for="user-account-image">Image</Label>
                  <AvInput id="user-account-image" type="select" className="form-control" name="image.id">
                    <option value="" key="0" />
                    {images
                      ? images.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="user-account-user">User</Label>
                  <AvInput id="user-account-user" type="select" className="form-control" name="user.id">
                    <option value="" key="0" />
                    {users
                      ? users.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="user-account-likedShout">Liked Shout</Label>
                  <AvInput
                    id="user-account-likedShout"
                    type="select"
                    multiple
                    className="form-control"
                    name="likedShouts"
                    value={userAccountEntity.likedShouts && userAccountEntity.likedShouts.map(e => e.id)}
                  >
                    <option value="" key="0" />
                    {shouts
                      ? shouts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="user-account-tag">Tag</Label>
                  <AvInput
                    id="user-account-tag"
                    type="select"
                    multiple
                    className="form-control"
                    name="tags"
                    value={userAccountEntity.tags && userAccountEntity.tags.map(e => e.id)}
                  >
                    <option value="" key="0" />
                    {tags
                      ? tags.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="user-account-listener">Listener</Label>
                  <AvInput
                    id="user-account-listener"
                    type="select"
                    multiple
                    className="form-control"
                    name="listeners"
                    value={userAccountEntity.listeners && userAccountEntity.listeners.map(e => e.id)}
                  >
                    <option value="" key="0" />
                    {userAccounts
                      ? userAccounts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/user-account" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  images: storeState.image.entities,
  users: storeState.userManagement.users,
  shouts: storeState.shout.entities,
  tags: storeState.tag.entities,
  userAccounts: storeState.userAccount.entities,
  userAccountEntity: storeState.userAccount.entity,
  loading: storeState.userAccount.loading,
  updating: storeState.userAccount.updating,
  updateSuccess: storeState.userAccount.updateSuccess
});

const mapDispatchToProps = {
  getImages,
  getUsers,
  getShouts,
  getTags,
  getUserAccounts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAccountUpdate);

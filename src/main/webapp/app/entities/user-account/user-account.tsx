import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './user-account.reducer';
import { IUserAccount } from 'app/shared/model/user-account.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserAccountProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class UserAccount extends React.Component<IUserAccountProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { userAccountList, match } = this.props;
    return (
      <div>
        <h2 id="user-account-heading">
          User Accounts
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new User Account
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Image</th>
                <th>User</th>
                <th>Liked Shout</th>
                <th>Tag</th>
                <th>Listener</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {userAccountList.map((userAccount, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${userAccount.id}`} color="link" size="sm">
                      {userAccount.id}
                    </Button>
                  </td>
                  <td>{userAccount.username}</td>
                  <td>{userAccount.firstName}</td>
                  <td>{userAccount.lastName}</td>
                  <td>{userAccount.image ? <Link to={`image/${userAccount.image.id}`}>{userAccount.image.id}</Link> : ''}</td>
                  <td>{userAccount.user ? userAccount.user.id : ''}</td>
                  <td>
                    {userAccount.likedShouts
                      ? userAccount.likedShouts.map((val, j) => (
                          <span key={j}>
                            <Link to={`shout/${val.id}`}>{val.id}</Link>
                            {j === userAccount.likedShouts.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td>
                    {userAccount.tags
                      ? userAccount.tags.map((val, j) => (
                          <span key={j}>
                            <Link to={`tag/${val.id}`}>{val.id}</Link>
                            {j === userAccount.tags.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td>
                    {userAccount.listeners
                      ? userAccount.listeners.map((val, j) => (
                          <span key={j}>
                            <Link to={`user-account/${val.id}`}>{val.id}</Link>
                            {j === userAccount.listeners.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${userAccount.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userAccount.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${userAccount.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ userAccount }: IRootState) => ({
  userAccountList: userAccount.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAccount);

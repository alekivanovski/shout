import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './shout.reducer';
import { IShout } from 'app/shared/model/shout.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IShoutDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class ShoutDetail extends React.Component<IShoutDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { shoutEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Shout [<b>{shoutEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="title">Title</span>
            </dt>
            <dd>{shoutEntity.title}</dd>
            <dt>
              <span id="description">Description</span>
            </dt>
            <dd>{shoutEntity.description}</dd>
            <dt>
              <span id="longitude">Longitude</span>
            </dt>
            <dd>{shoutEntity.longitude}</dd>
            <dt>
              <span id="latitude">Latitude</span>
            </dt>
            <dd>{shoutEntity.latitude}</dd>
            <dt>
              <span id="dateCreated">Date Created</span>
            </dt>
            <dd>
              <TextFormat value={shoutEntity.dateCreated} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>Image</dt>
            <dd>{shoutEntity.image ? shoutEntity.image.id : ''}</dd>
            <dt>Owner</dt>
            <dd>{shoutEntity.owner ? shoutEntity.owner.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/shout" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/shout/${shoutEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ shout }: IRootState) => ({
  shoutEntity: shout.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoutDetail);

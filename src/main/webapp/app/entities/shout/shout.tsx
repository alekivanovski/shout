import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './shout.reducer';
import { IShout } from 'app/shared/model/shout.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IShoutProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Shout extends React.Component<IShoutProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { shoutList, match } = this.props;
    return (
      <div>
        <h2 id="shout-heading">
          Shouts
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create new Shout
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Description</th>
                <th>Longitude</th>
                <th>Latitude</th>
                <th>Date Created</th>
                <th>Image</th>
                <th>Owner</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {shoutList.map((shout, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${shout.id}`} color="link" size="sm">
                      {shout.id}
                    </Button>
                  </td>
                  <td>{shout.title}</td>
                  <td>{shout.description}</td>
                  <td>{shout.longitude}</td>
                  <td>{shout.latitude}</td>
                  <td>
                    <TextFormat type="date" value={shout.dateCreated} format={APP_DATE_FORMAT} />
                  </td>
                  <td>{shout.image ? <Link to={`image/${shout.image.id}`}>{shout.image.id}</Link> : ''}</td>
                  <td>{shout.owner ? <Link to={`user-account/${shout.owner.id}`}>{shout.owner.id}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${shout.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${shout.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${shout.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ shout }: IRootState) => ({
  shoutList: shout.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Shout);

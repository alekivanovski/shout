import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IShout, defaultValue } from 'app/shared/model/shout.model';

export const ACTION_TYPES = {
  FETCH_SHOUT_LIST: 'shout/FETCH_SHOUT_LIST',
  FETCH_SHOUT: 'shout/FETCH_SHOUT',
  CREATE_SHOUT: 'shout/CREATE_SHOUT',
  UPDATE_SHOUT: 'shout/UPDATE_SHOUT',
  DELETE_SHOUT: 'shout/DELETE_SHOUT',
  RESET: 'shout/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IShout>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type ShoutState = Readonly<typeof initialState>;

// Reducer

export default (state: ShoutState = initialState, action): ShoutState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_SHOUT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_SHOUT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_SHOUT):
    case REQUEST(ACTION_TYPES.UPDATE_SHOUT):
    case REQUEST(ACTION_TYPES.DELETE_SHOUT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_SHOUT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_SHOUT):
    case FAILURE(ACTION_TYPES.CREATE_SHOUT):
    case FAILURE(ACTION_TYPES.UPDATE_SHOUT):
    case FAILURE(ACTION_TYPES.DELETE_SHOUT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_SHOUT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_SHOUT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_SHOUT):
    case SUCCESS(ACTION_TYPES.UPDATE_SHOUT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_SHOUT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/shouts';

// Actions

export const getEntities: ICrudGetAllAction<IShout> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_SHOUT_LIST,
  payload: axios.get<IShout>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IShout> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_SHOUT,
    payload: axios.get<IShout>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IShout> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_SHOUT,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IShout> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_SHOUT,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IShout> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_SHOUT,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});

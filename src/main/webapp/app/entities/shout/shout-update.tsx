import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IImage } from 'app/shared/model/image.model';
import { getEntities as getImages } from 'app/entities/image/image.reducer';
import { IUserAccount } from 'app/shared/model/user-account.model';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { getEntity, updateEntity, createEntity, reset } from './shout.reducer';
import { IShout } from 'app/shared/model/shout.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IShoutUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IShoutUpdateState {
  isNew: boolean;
  imageId: string;
  ownerId: string;
  likerId: string;
}

export class ShoutUpdate extends React.Component<IShoutUpdateProps, IShoutUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      imageId: '0',
      ownerId: '0',
      likerId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getImages();
    this.props.getUserAccounts();
  }

  saveEntity = (event, errors, values) => {
    values.dateCreated = convertDateTimeToServer(values.dateCreated);

    if (errors.length === 0) {
      const { shoutEntity } = this.props;
      const entity = {
        ...shoutEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/shout');
  };

  render() {
    const { shoutEntity, images, userAccounts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="shoutApp.shout.home.createOrEditLabel">Create or edit a Shout</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : shoutEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="shout-id">ID</Label>
                    <AvInput id="shout-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="titleLabel" for="shout-title">
                    Title
                  </Label>
                  <AvField id="shout-title" type="text" name="title" />
                </AvGroup>
                <AvGroup>
                  <Label id="descriptionLabel" for="shout-description">
                    Description
                  </Label>
                  <AvField id="shout-description" type="text" name="description" />
                </AvGroup>
                <AvGroup>
                  <Label id="longitudeLabel" for="shout-longitude">
                    Longitude
                  </Label>
                  <AvField id="shout-longitude" type="string" className="form-control" name="longitude" />
                </AvGroup>
                <AvGroup>
                  <Label id="latitudeLabel" for="shout-latitude">
                    Latitude
                  </Label>
                  <AvField id="shout-latitude" type="string" className="form-control" name="latitude" />
                </AvGroup>
                <AvGroup>
                  <Label id="dateCreatedLabel" for="shout-dateCreated">
                    Date Created
                  </Label>
                  <AvInput
                    id="shout-dateCreated"
                    type="datetime-local"
                    className="form-control"
                    name="dateCreated"
                    placeholder={'YYYY-MM-DD HH:mm'}
                    value={isNew ? null : convertDateTimeFromServer(this.props.shoutEntity.dateCreated)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="shout-image">Image</Label>
                  <AvInput id="shout-image" type="select" className="form-control" name="image.id">
                    <option value="" key="0" />
                    {images
                      ? images.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="shout-owner">Owner</Label>
                  <AvInput id="shout-owner" type="select" className="form-control" name="owner.id">
                    <option value="" key="0" />
                    {userAccounts
                      ? userAccounts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/shout" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  images: storeState.image.entities,
  userAccounts: storeState.userAccount.entities,
  shoutEntity: storeState.shout.entity,
  loading: storeState.shout.loading,
  updating: storeState.shout.updating,
  updateSuccess: storeState.shout.updateSuccess
});

const mapDispatchToProps = {
  getImages,
  getUserAccounts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShoutUpdate);

import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Shout from './shout';
import ShoutDetail from './shout-detail';
import ShoutUpdate from './shout-update';
import ShoutDeleteDialog from './shout-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={ShoutUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={ShoutUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={ShoutDetail} />
      <ErrorBoundaryRoute path={match.url} component={Shout} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={ShoutDeleteDialog} />
  </>
);

export default Routes;

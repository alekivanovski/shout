import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IShout } from 'app/shared/model/shout.model';
import { getEntities as getShouts } from 'app/entities/shout/shout.reducer';
import { IUserAccount } from 'app/shared/model/user-account.model';
import { getEntities as getUserAccounts } from 'app/entities/user-account/user-account.reducer';
import { getEntity, updateEntity, createEntity, reset } from './tag.reducer';
import { ITag } from 'app/shared/model/tag.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ITagUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface ITagUpdateState {
  isNew: boolean;
  shoutId: string;
  userAccountId: string;
}

export class TagUpdate extends React.Component<ITagUpdateProps, ITagUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      shoutId: '0',
      userAccountId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getShouts();
    this.props.getUserAccounts();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { tagEntity } = this.props;
      const entity = {
        ...tagEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/tag');
  };

  render() {
    const { tagEntity, shouts, userAccounts, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="shoutApp.tag.home.createOrEditLabel">Create or edit a Tag</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : tagEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="tag-id">ID</Label>
                    <AvInput id="tag-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="tagNameLabel" for="tag-tagName">
                    Tag Name
                  </Label>
                  <AvField id="tag-tagName" type="text" name="tagName" />
                </AvGroup>
                <AvGroup>
                  <Label for="tag-shout">Shout</Label>
                  <AvInput id="tag-shout" type="select" className="form-control" name="shout.id">
                    <option value="" key="0" />
                    {shouts
                      ? shouts.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/tag" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  shouts: storeState.shout.entities,
  userAccounts: storeState.userAccount.entities,
  tagEntity: storeState.tag.entity,
  loading: storeState.tag.loading,
  updating: storeState.tag.updating,
  updateSuccess: storeState.tag.updateSuccess
});

const mapDispatchToProps = {
  getShouts,
  getUserAccounts,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagUpdate);

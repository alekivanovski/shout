import { IShout } from 'app/shared/model/shout.model';
import { IUserAccount } from 'app/shared/model/user-account.model';

export interface ITag {
  id?: number;
  tagName?: string;
  shout?: IShout;
  userAccounts?: IUserAccount[];
}

export const defaultValue: Readonly<ITag> = {};

export interface IImage {
  id?: number;
  imageDataContentType?: string;
  imageData?: any;
}

export const defaultValue: Readonly<IImage> = {};

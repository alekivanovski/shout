import { IImage } from 'app/shared/model/image.model';
import { IUser } from 'app/shared/model/user.model';
import { IComment } from 'app/shared/model/comment.model';
import { IShout } from 'app/shared/model/shout.model';
import { ITag } from 'app/shared/model/tag.model';
import { IUserAccount } from 'app/shared/model/user-account.model';

export interface IUserAccount {
  id?: number;
  username?: string;
  firstName?: string;
  lastName?: string;
  image?: IImage;
  user?: IUser;
  comments?: IComment[];
  myShouts?: IShout[];
  likedShouts?: IShout[];
  tags?: ITag[];
  listeners?: IUserAccount[];
  listensTos?: IUserAccount[];
}

export const defaultValue: Readonly<IUserAccount> = {};

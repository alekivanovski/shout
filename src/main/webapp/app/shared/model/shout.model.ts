import { Moment } from 'moment';
import { IImage } from 'app/shared/model/image.model';
import { IComment } from 'app/shared/model/comment.model';
import { ITag } from 'app/shared/model/tag.model';
import { IUserAccount } from 'app/shared/model/user-account.model';

export interface IShout {
  id?: number;
  title?: string;
  description?: string;
  longitude?: number;
  latitude?: number;
  dateCreated?: Moment;
  image?: IImage;
  comments?: IComment[];
  tags?: ITag[];
  owner?: IUserAccount;
  likers?: IUserAccount[];
}

export const defaultValue: Readonly<IShout> = {};

import { IUserAccount } from 'app/shared/model/user-account.model';
import { IShout } from 'app/shared/model/shout.model';

export interface IComment {
  id?: number;
  comment?: string;
  userAccount?: IUserAccount;
  shout?: IShout;
}

export const defaultValue: Readonly<IComment> = {};

/**
 * View Models used by Spring MVC REST controllers.
 */
package mk.ukim.finki.web.rest.vm;

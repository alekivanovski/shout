package mk.ukim.finki.web.rest;

import mk.ukim.finki.domain.Shout;
import mk.ukim.finki.repository.ShoutRepository;
import mk.ukim.finki.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link mk.ukim.finki.domain.Shout}.
 */
@RestController
@RequestMapping("/api")
public class ShoutResource {

    private final Logger log = LoggerFactory.getLogger(ShoutResource.class);

    private static final String ENTITY_NAME = "shout";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ShoutRepository shoutRepository;

    public ShoutResource(ShoutRepository shoutRepository) {
        this.shoutRepository = shoutRepository;
    }

    /**
     * {@code POST  /shouts} : Create a new shout.
     *
     * @param shout the shout to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new shout, or with status {@code 400 (Bad Request)} if the shout has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/shouts")
    public ResponseEntity<Shout> createShout(@RequestBody Shout shout) throws URISyntaxException {
        log.debug("REST request to save Shout : {}", shout);
        if (shout.getId() != null) {
            throw new BadRequestAlertException("A new shout cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Shout result = shoutRepository.save(shout);
        return ResponseEntity.created(new URI("/api/shouts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /shouts} : Updates an existing shout.
     *
     * @param shout the shout to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated shout,
     * or with status {@code 400 (Bad Request)} if the shout is not valid,
     * or with status {@code 500 (Internal Server Error)} if the shout couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/shouts")
    public ResponseEntity<Shout> updateShout(@RequestBody Shout shout) throws URISyntaxException {
        log.debug("REST request to update Shout : {}", shout);
        if (shout.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Shout result = shoutRepository.save(shout);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, shout.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /shouts} : get all the shouts.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of shouts in body.
     */
    @GetMapping("/shouts")
    public List<Shout> getAllShouts() {
        log.debug("REST request to get all Shouts");
        return shoutRepository.findAll();
    }

    /**
     * {@code GET  /shouts/:id} : get the "id" shout.
     *
     * @param id the id of the shout to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the shout, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/shouts/{id}")
    public ResponseEntity<Shout> getShout(@PathVariable Long id) {
        log.debug("REST request to get Shout : {}", id);
        Optional<Shout> shout = shoutRepository.findOneWithCommentsById(id);
        return ResponseUtil.wrapOrNotFound(shout);
    }

    /**
     * {@code DELETE  /shouts/:id} : delete the "id" shout.
     *
     * @param id the id of the shout to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/shouts/{id}")
    public ResponseEntity<Void> deleteShout(@PathVariable Long id) {
        log.debug("REST request to delete Shout : {}", id);
        shoutRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}

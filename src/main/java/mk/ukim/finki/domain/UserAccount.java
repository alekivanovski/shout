package mk.ukim.finki.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A UserAccount.
 */
@Entity
@Table(name = "user_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "username")
    private String username;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToOne
    @JoinColumn(unique = true)
    private Image image;

    @OneToOne
    @JoinColumn(unique = true)
    private User user;

    @OneToMany(mappedBy = "userAccount")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();

    @OneToMany(mappedBy = "owner")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Shout> myShouts = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "user_account_liked_shout",
               joinColumns = @JoinColumn(name = "user_account_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "liked_shout_id", referencedColumnName = "id"))
    private Set<Shout> likedShouts = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "user_account_tag",
               joinColumns = @JoinColumn(name = "user_account_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "user_account_listener",
               joinColumns = @JoinColumn(name = "user_account_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "listener_id", referencedColumnName = "id"))
    private Set<UserAccount> listeners = new HashSet<>();

    @ManyToMany(mappedBy = "listeners")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<UserAccount> listensTos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public UserAccount username(String username) {
        this.username = username;
        return this;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public UserAccount firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UserAccount lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Image getImage() {
        return image;
    }

    public UserAccount image(Image image) {
        this.image = image;
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public User getUser() {
        return user;
    }

    public UserAccount user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public UserAccount comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public UserAccount addComment(Comment comment) {
        this.comments.add(comment);
        comment.setUserAccount(this);
        return this;
    }

    public UserAccount removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setUserAccount(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Shout> getMyShouts() {
        return myShouts;
    }

    public UserAccount myShouts(Set<Shout> shouts) {
        this.myShouts = shouts;
        return this;
    }

    public UserAccount addMyShout(Shout shout) {
        this.myShouts.add(shout);
        shout.setOwner(this);
        return this;
    }

    public UserAccount removeMyShout(Shout shout) {
        this.myShouts.remove(shout);
        shout.setOwner(null);
        return this;
    }

    public void setMyShouts(Set<Shout> shouts) {
        this.myShouts = shouts;
    }

    public Set<Shout> getLikedShouts() {
        return likedShouts;
    }

    public UserAccount likedShouts(Set<Shout> shouts) {
        this.likedShouts = shouts;
        return this;
    }

    public UserAccount addLikedShout(Shout shout) {
        this.likedShouts.add(shout);
        shout.getLikers().add(this);
        return this;
    }

    public UserAccount removeLikedShout(Shout shout) {
        this.likedShouts.remove(shout);
        shout.getLikers().remove(this);
        return this;
    }

    public void setLikedShouts(Set<Shout> shouts) {
        this.likedShouts = shouts;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public UserAccount tags(Set<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public UserAccount addTag(Tag tag) {
        this.tags.add(tag);
        tag.getUserAccounts().add(this);
        return this;
    }

    public UserAccount removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.getUserAccounts().remove(this);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Set<UserAccount> getListeners() {
        return listeners;
    }

    public UserAccount listeners(Set<UserAccount> userAccounts) {
        this.listeners = userAccounts;
        return this;
    }

    public UserAccount addListener(UserAccount userAccount) {
        this.listeners.add(userAccount);
        userAccount.getListensTos().add(this);
        return this;
    }

    public UserAccount removeListener(UserAccount userAccount) {
        this.listeners.remove(userAccount);
        userAccount.getListensTos().remove(this);
        return this;
    }

    public void setListeners(Set<UserAccount> userAccounts) {
        this.listeners = userAccounts;
    }

    public Set<UserAccount> getListensTos() {
        return listensTos;
    }

    public UserAccount listensTos(Set<UserAccount> userAccounts) {
        this.listensTos = userAccounts;
        return this;
    }

    public UserAccount addListensTo(UserAccount userAccount) {
        this.listensTos.add(userAccount);
        userAccount.getListeners().add(this);
        return this;
    }

    public UserAccount removeListensTo(UserAccount userAccount) {
        this.listensTos.remove(userAccount);
        userAccount.getListeners().remove(this);
        return this;
    }

    public void setListensTos(Set<UserAccount> userAccounts) {
        this.listensTos = userAccounts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof UserAccount)) {
            return false;
        }
        return id != null && id.equals(((UserAccount) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
            "id=" + getId() +
            ", username='" + getUsername() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            "}";
    }
}

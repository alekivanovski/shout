package mk.ukim.finki.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Shout.
 */
@Entity
@Table(name = "shout")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Shout implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "date_created")
    private ZonedDateTime dateCreated;

    @OneToOne
    @JoinColumn(unique = true)
    private Image image;

    @OneToMany(mappedBy = "shout", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Comment> comments = new HashSet<>();

    @OneToMany(mappedBy = "shout")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Tag> tags = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = "myShouts", allowSetters = true)
    private UserAccount owner;

    @ManyToMany(mappedBy = "likedShouts")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JsonIgnore
    private Set<UserAccount> likers = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Shout title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public Shout description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getLongitude() {
        return longitude;
    }

    public Shout longitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Shout latitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public ZonedDateTime getDateCreated() {
        return dateCreated;
    }

    public Shout dateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
        return this;
    }

    public void setDateCreated(ZonedDateTime dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Image getImage() {
        return image;
    }

    public Shout image(Image image) {
        this.image = image;
        return this;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Set<Comment> getComments() {
        return comments;
    }

    public Shout comments(Set<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public Shout addComment(Comment comment) {
        this.comments.add(comment);
        comment.setShout(this);
        return this;
    }

    public Shout removeComment(Comment comment) {
        this.comments.remove(comment);
        comment.setShout(null);
        return this;
    }

    public void setComments(Set<Comment> comments) {
        this.comments = comments;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public Shout tags(Set<Tag> tags) {
        this.tags = tags;
        return this;
    }

    public Shout addTag(Tag tag) {
        this.tags.add(tag);
        tag.setShout(this);
        return this;
    }

    public Shout removeTag(Tag tag) {
        this.tags.remove(tag);
        tag.setShout(null);
        return this;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public UserAccount getOwner() {
        return owner;
    }

    public Shout owner(UserAccount userAccount) {
        this.owner = userAccount;
        return this;
    }

    public void setOwner(UserAccount userAccount) {
        this.owner = userAccount;
    }

    public Set<UserAccount> getLikers() {
        return likers;
    }

    public Shout likers(Set<UserAccount> userAccounts) {
        this.likers = userAccounts;
        return this;
    }

    public Shout addLiker(UserAccount userAccount) {
        this.likers.add(userAccount);
        userAccount.getLikedShouts().add(this);
        return this;
    }

    public Shout removeLiker(UserAccount userAccount) {
        this.likers.remove(userAccount);
        userAccount.getLikedShouts().remove(this);
        return this;
    }

    public void setLikers(Set<UserAccount> userAccounts) {
        this.likers = userAccounts;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Shout)) {
            return false;
        }
        return id != null && id.equals(((Shout) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Shout{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", longitude=" + getLongitude() +
            ", latitude=" + getLatitude() +
            ", dateCreated='" + getDateCreated() + "'" +
            "}";
    }
}

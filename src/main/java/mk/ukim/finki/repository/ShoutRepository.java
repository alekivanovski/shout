package mk.ukim.finki.repository;

import mk.ukim.finki.domain.Shout;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Shout entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ShoutRepository extends JpaRepository<Shout, Long> {

    Optional<Shout> findOneWithCommentsById(Long id);
}

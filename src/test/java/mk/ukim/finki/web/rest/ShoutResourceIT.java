package mk.ukim.finki.web.rest;

import mk.ukim.finki.ShoutApp;
import mk.ukim.finki.domain.Shout;
import mk.ukim.finki.repository.ShoutRepository;
import mk.ukim.finki.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static mk.ukim.finki.web.rest.TestUtil.sameInstant;
import static mk.ukim.finki.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link ShoutResource} REST controller.
 */
@SpringBootTest(classes = ShoutApp.class)
public class ShoutResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final ZonedDateTime DEFAULT_DATE_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private ShoutRepository shoutRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restShoutMockMvc;

    private Shout shout;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ShoutResource shoutResource = new ShoutResource(shoutRepository);
        this.restShoutMockMvc = MockMvcBuilders.standaloneSetup(shoutResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shout createEntity(EntityManager em) {
        Shout shout = new Shout()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .longitude(DEFAULT_LONGITUDE)
            .latitude(DEFAULT_LATITUDE)
            .dateCreated(DEFAULT_DATE_CREATED);
        return shout;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Shout createUpdatedEntity(EntityManager em) {
        Shout shout = new Shout()
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .longitude(UPDATED_LONGITUDE)
            .latitude(UPDATED_LATITUDE)
            .dateCreated(UPDATED_DATE_CREATED);
        return shout;
    }

    @BeforeEach
    public void initTest() {
        shout = createEntity(em);
    }

    @Test
    @Transactional
    public void createShout() throws Exception {
        int databaseSizeBeforeCreate = shoutRepository.findAll().size();

        // Create the Shout
        restShoutMockMvc.perform(post("/api/shouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shout)))
            .andExpect(status().isCreated());

        // Validate the Shout in the database
        List<Shout> shoutList = shoutRepository.findAll();
        assertThat(shoutList).hasSize(databaseSizeBeforeCreate + 1);
        Shout testShout = shoutList.get(shoutList.size() - 1);
        assertThat(testShout.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testShout.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testShout.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testShout.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testShout.getDateCreated()).isEqualTo(DEFAULT_DATE_CREATED);
    }

    @Test
    @Transactional
    public void createShoutWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = shoutRepository.findAll().size();

        // Create the Shout with an existing ID
        shout.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restShoutMockMvc.perform(post("/api/shouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shout)))
            .andExpect(status().isBadRequest());

        // Validate the Shout in the database
        List<Shout> shoutList = shoutRepository.findAll();
        assertThat(shoutList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllShouts() throws Exception {
        // Initialize the database
        shoutRepository.saveAndFlush(shout);

        // Get all the shoutList
        restShoutMockMvc.perform(get("/api/shouts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(shout.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].dateCreated").value(hasItem(sameInstant(DEFAULT_DATE_CREATED))));
    }
    
    @Test
    @Transactional
    public void getShout() throws Exception {
        // Initialize the database
        shoutRepository.saveAndFlush(shout);

        // Get the shout
        restShoutMockMvc.perform(get("/api/shouts/{id}", shout.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(shout.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.dateCreated").value(sameInstant(DEFAULT_DATE_CREATED)));
    }

    @Test
    @Transactional
    public void getNonExistingShout() throws Exception {
        // Get the shout
        restShoutMockMvc.perform(get("/api/shouts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateShout() throws Exception {
        // Initialize the database
        shoutRepository.saveAndFlush(shout);

        int databaseSizeBeforeUpdate = shoutRepository.findAll().size();

        // Update the shout
        Shout updatedShout = shoutRepository.findById(shout.getId()).get();
        // Disconnect from session so that the updates on updatedShout are not directly saved in db
        em.detach(updatedShout);
        updatedShout
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .longitude(UPDATED_LONGITUDE)
            .latitude(UPDATED_LATITUDE)
            .dateCreated(UPDATED_DATE_CREATED);

        restShoutMockMvc.perform(put("/api/shouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedShout)))
            .andExpect(status().isOk());

        // Validate the Shout in the database
        List<Shout> shoutList = shoutRepository.findAll();
        assertThat(shoutList).hasSize(databaseSizeBeforeUpdate);
        Shout testShout = shoutList.get(shoutList.size() - 1);
        assertThat(testShout.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testShout.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testShout.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testShout.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testShout.getDateCreated()).isEqualTo(UPDATED_DATE_CREATED);
    }

    @Test
    @Transactional
    public void updateNonExistingShout() throws Exception {
        int databaseSizeBeforeUpdate = shoutRepository.findAll().size();

        // Create the Shout

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restShoutMockMvc.perform(put("/api/shouts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(shout)))
            .andExpect(status().isBadRequest());

        // Validate the Shout in the database
        List<Shout> shoutList = shoutRepository.findAll();
        assertThat(shoutList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteShout() throws Exception {
        // Initialize the database
        shoutRepository.saveAndFlush(shout);

        int databaseSizeBeforeDelete = shoutRepository.findAll().size();

        // Delete the shout
        restShoutMockMvc.perform(delete("/api/shouts/{id}", shout.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Shout> shoutList = shoutRepository.findAll();
        assertThat(shoutList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Shout.class);
        Shout shout1 = new Shout();
        shout1.setId(1L);
        Shout shout2 = new Shout();
        shout2.setId(shout1.getId());
        assertThat(shout1).isEqualTo(shout2);
        shout2.setId(2L);
        assertThat(shout1).isNotEqualTo(shout2);
        shout1.setId(null);
        assertThat(shout1).isNotEqualTo(shout2);
    }
}
